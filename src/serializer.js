import escapeHtml from 'escape-html'
import generateEquationImage from "./ktoc";

export const htmlForWebSerializer = async (node, appDiv, questionId=null) => {
  const ASSETS_BASE_URL="https://dev-testpaper-assets.innovatetech.io"

  if (node.object === 'text') {
    const text = escapeHtml(node.text) || '&#65279;'
    if (node.marks) {
      return node.marks.reduce(serializeHtmlMarks, text)
    }
    return text
  }
  const children = node
    ? (await Promise.all(node.nodes.map((n) => htmlForWebSerializer(n, appDiv, questionId)))).join('')
    : ''

  const align = node.data.align
  const style = align ? `style="text-align:${align};"` : ''

  if (node.object === 'inline') {
    switch (node.type) {
      case 'katex':
        const imgsrc = generateEquationImage(node.data.math, appDiv, questionId)
        return `<span class="latexImageWrapperInline"><img src="${imgsrc}"/></span>`
      default:
        return children
    }
  }

  switch (node.type) {
    case 'paragraph':
      return `<p ${style}>${children}</p>`
    case 'table':
      return `<table><tbody>${children}</tbody></table>`
    case 'table_row':
      return `<tr>${children}</tr>`
    case 'table_cell':
      return `<td>${children}</td>`
    case 'image':
      const src = `${ASSETS_BASE_URL}/${node.data.src}`
      return `<div class="imageWrapper"><img src="${src}"/></div>`
    case 'katex':
      // convert latex to image
      const imgsrc = await generateEquationImage(appDiv, node.data.math)
      // const imgsrc = await convertLatexToPng(node.data.math, questionId)
      return `<div class="latexImageWrapper"><img src="${imgsrc}"/></div>`
    case 'line':
      const borderStyle = node.data.style === 'solid' ? '1px solid' : '2px dotted'
      return `<table width="100%" style="border:0"><tr style="border:0"><td style="border: 0; border-bottom: ${borderStyle} #000;">&#65279;</td></tr></table>`
    default:
      return children
  }
}


const serializeHtmlMarks = (text, mark) => {
  switch (mark.type) {
    case 'bold':
      return `<strong>${text}</strong>`
    case 'italic':
      return `<i>${text}</i>`
    case 'underline':
      return `<u>${text}</u>`
    case 'sup':
      return `<sup>${text}</sup>`
    case 'sub':
      return `<sub>${text}</sub>`
    default:
      return text
  }
}