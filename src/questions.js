const questions = [
    [
      {
        "object": "value",
        "document": {
          "object": "document",
          "data": {},
          "nodes": [
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "Show that ", "marks": [] }]
            },
            {
              "object": "block",
              "type": "katex",
              "data": { "math": "{{sqrt24 xxsqrt27 +9sqrt30/sqrt15}}" },
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [
                {
                  "object": "text",
                  "text": " can be written in the form a√2, where a is a n integer.",
                  "marks": []
                }
              ]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            }
          ]
        }
      },
      {
        "object": "value",
        "document": {
          "object": "document",
          "data": {},
          "nodes": [
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [
                { "object": "text", "text": "Solve the equation ", "marks": [] }
              ]
            },
            {
              "object": "block",
              "type": "katex",
              "data": { "math": "{{sqrt3(1+x)=2(x-3)}}" },
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [
                {
                  "object": "text",
                  "text": ", giving your answer in the form ",
                  "marks": []
                }
              ]
            },
            {
              "object": "block",
              "type": "katex",
              "data": { "math": "{{b+csqrt3}}" },
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [
                {
                  "object": "text",
                  "text": ", where b and c are integer.",
                  "marks": []
                }
              ]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            }
          ]
        }
      }
    ],
    [
      ({
        "object": "value",
        "document": {
          "object": "document",
          "data": {},
          "nodes": [
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [
                { "object": "text", "text": "Draw the graph of ", "marks": [] }
              ]
            },
            {
              "object": "block",
              "type": "katex",
              "data": { "math": "{{lnP}}" },
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": " against ", "marks": [] }]
            },
            {
              "object": "block",
              "type": "katex",
              "data": { "math": "{{t}}" },
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [
                { "object": "text", "text": " on the grid below.", "marks": [] }
              ]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "image",
              "data": { "src": "0dbf1f3b-5592-4726-b254-3e0cfea53075.jpg" },
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            }
          ]
        }
      },
      {
        "object": "value",
        "document": {
          "object": "document",
          "data": {},
          "nodes": [
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [
                {
                  "object": "text",
                  "text": "Given that your equation in part (iv) is valid for values of t up to 10, find the smallest value of t, correct to 1 decimal place, for which P is at least 1000. ",
                  "marks": []
                }
              ]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            }
          ]
        }
      },
      {
        "object": "value",
        "document": {
          "object": "document",
          "data": {},
          "nodes": [
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "Prove that ", "marks": [] }]
            },
            {
              "object": "block",
              "type": "katex",
              "data": { "math": "{{sinx(cotx+tanx)=secx}}" },
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            }
          ]
        }
      },
      {
        "object": "value",
        "document": {
          "object": "document",
          "data": {},
          "nodes": [
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [
                {
                  "object": "text",
                  "text": "Use the graph to estimate the value of P when t = 2.2.",
                  "marks": []
                }
              ]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            }
          ]
        }
      },
      {
        "object": "value",
        "document": {
          "object": "document",
          "data": {},
          "nodes": [
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [
                {
                  "object": "text",
                  "text": "Find the gradient of the graph and state the coordinates of the point where the graph meets the vertical axis.",
                  "marks": []
                }
              ]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            }
          ]
        }
      },
      {
        "object": "value",
        "document": {
          "object": "document",
          "data": {},
          "nodes": [
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [
                {
                  "object": "text",
                  "text": "Using your answers to part (iii), show that ",
                  "marks": []
                }
              ]
            },
            {
              "object": "block",
              "type": "katex",
              "data": { "math": "{{P=ab^t}}" },
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [
                {
                  "object": "text",
                  "text": ", where a and b are constants to be found. ",
                  "marks": []
                }
              ]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            }
          ]
        }
      },
      {
        "object": "value",
        "document": {
          "object": "document",
          "data": {},
          "nodes": [
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [
                {
                  "object": "text",
                  "text": "Hence solve the equation ",
                  "marks": []
                }
              ]
            },
            {
              "object": "block",
              "type": "katex",
              "data": { "math": "{{|sinx(cotx+tanx)|=2}}" },
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": " for ", "marks": [] }]
            },
            {
              "object": "block",
              "type": "katex",
              "data": { "math": "{{0°≤ x ≤ 360°}}" },
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            }
          ]
        }
      })
    ],
    [
      ({
        "object": "value",
        "document": {
          "object": "document",
          "data": {},
          "nodes": [
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [
                {
                  "object": "text",
                  "text": "The table shows values of the variables t and P.",
                  "marks": []
                }
              ]
            },
            {
              "object": "block",
              "type": "image",
              "data": { "src": "2dd1b64b-e639-4966-a707-02a5b750fe2d.jpg" },
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            }
          ]
        }
      },
      {
        "object": "value",
        "document": {
          "object": "document",
          "data": {},
          "nodes": [
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [
                {
                  "object": "text",
                  "text": "The velocity, v m/s, of a particle travelling in a straight line, t seconds after passing through a fixed point O, is given by ",
                  "marks": []
                }
              ]
            },
            {
              "object": "block",
              "type": "katex",
              "data": { "math": "{{v=4/(t+1)^3}}" },
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            }
          ]
        }
      },
      {
        "object": "value",
        "document": {
          "object": "document",
          "data": {},
          "nodes": [
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [
                {
                  "object": "text",
                  "text": "Do not use calculator in this question.",
                  "marks": []
                }
              ]
            }
          ]
        }
      },
      {
        "object": "value",
        "document": {
          "object": "document",
          "data": {},
          "nodes": [
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "Solve ", "marks": [] }]
            },
            {
              "object": "block",
              "type": "katex",
              "data": { "math": "{{e^(2x+1) = 3 e^(4-3x)}}" },
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            }
          ]
        }
      },
      {
        "object": "value",
        "document": {
          "object": "document",
          "data": {},
          "nodes": [
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [
                {
                  "object": "text",
                  "text": "On the axes below, draw the graph of ",
                  "marks": []
                }
              ]
            },
            {
              "object": "block",
              "type": "katex",
              "data": { "math": "{{y=|2x-3|}}" },
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "paragraph",
              "data": {},
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            },
            {
              "object": "block",
              "type": "image",
              "data": { "src": "896c7084-4822-45aa-aa02-35f1509ac976.jpg" },
              "nodes": [{ "object": "text", "text": "", "marks": [] }]
            }
          ]
        }
      })
    ]
  ]
  
  export default questions