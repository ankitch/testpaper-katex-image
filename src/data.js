const data = {
  object: "value",
  document: {
    object: "document",
    data: {},
    nodes: [
      {
        object: "block",
        type: "paragraph",
        data: {},
        nodes: [{ object: "text", text: "This is a question", marks: [] }],
      },
      {
        object: "block",
        type: "paragraph",
        data: {},
        nodes: [{ object: "text", text: "", marks: [] }],
      },
      {
        object: "block",
        type: "table",
        data: {},
        nodes: [
          {
            object: "block",
            type: "table_row",
            data: {},
            nodes: [
              {
                object: "block",
                type: "table_cell",
                data: {},
                nodes: [
                  {
                    object: "block",
                    type: "paragraph",
                    data: {},
                    nodes: [{ object: "text", text: "", marks: [] }],
                  },
                ],
              },
              {
                object: "block",
                type: "table_cell",
                data: {},
                nodes: [
                  {
                    object: "block",
                    type: "paragraph",
                    data: {},
                    nodes: [{ object: "text", text: "hello", marks: [] }],
                  },
                ],
              },
            ],
          },
          {
            object: "block",
            type: "table_row",
            data: {},
            nodes: [
              {
                object: "block",
                type: "table_cell",
                data: {},
                nodes: [
                  {
                    object: "block",
                    type: "paragraph",
                    data: {},
                    nodes: [{ object: "text", text: "", marks: [] }],
                  },
                  {
                    object: "block",
                    type: "katex",
                    data: { math: "\\frac{1}{2}" },
                    nodes: [{ object: "text", text: "", marks: [] }],
                  },
                ],
              },
              {
                object: "block",
                type: "table_cell",
                data: {},
                nodes: [
                  {
                    object: "block",
                    type: "paragraph",
                    data: {},
                    nodes: [{ object: "text", text: "", marks: [] }],
                  },
                ],
              },
            ],
          },
        ],
      },
      {
        object: "block",
        type: "paragraph",
        data: {},
        nodes: [{ object: "text", text: "another paragraph", marks: [] }],
      },
      {
        object: "block",
        type: "paragraph",
        data: { align: "center" },
        nodes: [{ object: "text", text: "hello ", marks: [] }],
      },
      {
        object: "block",
        type: "paragraph",
        data: { align: "center" },
        nodes: [{ object: "text", text: "", marks: [] }],
      },
      {
        object: "block",
        type: "katex",
        data: { math: "\\frac{1}{2}" },
        nodes: [{ object: "text", text: "", marks: [] }],
      },
      {
        object: "block",
        type: "paragraph",
        data: { align: "center" },
        nodes: [{ object: "text", text: "", marks: [] }],
      },
    ],
  },
};

export default data