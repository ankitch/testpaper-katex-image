import _ from "lodash";
import generateEquationImage from "./ktoc";
import data from "./data";
import { htmlForWebSerializer } from "./serializer";
import questions  from './questions'

const appDiv = document.getElementById("app");

var stringToHTML = function (str) {
  var parser = new DOMParser();
  var doc = parser.parseFromString(str, "text/html");
  return doc.body;
};

const dd = `c = \\pm\\sqrt{a^2 + b^2} \\newline
Eq1 = 1.23 * 5^5 * 5.3 \\newline
Eq2 = 3.21 * 2^2 * 3.5 \\newline`;

const app = async () => {
  questions.map(data => {
    data.map(async(dt) => {
      const question = await htmlForWebSerializer(dt.document, appDiv, null);
      appDiv.appendChild(stringToHTML(question));
    })
  })

  // const question = await htmlForWebSerializer(data.document, appDiv, null);
  // appDiv.appendChild(stringToHTML(question));
};

app();