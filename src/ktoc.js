import katex from "katex";
import html2canvas from "html2canvas";

const generateEquationImage = async (app, data) => {
  const katexEquation = document.createElement("DIV")
  app.appendChild(katexEquation);
  katexEquation.style.display = 'inline-block'

  const equation = data
  const kateRendered = katex.renderToString(equation, {
    throwOnError: false,
  });
  const image = new Image();
  let equationImage
  katexEquation.innerHTML = kateRendered;

  if (katexEquation) {
    await html2canvas(katexEquation, {scale: '2'}).then(function (canvas) {
      equationImage = canvas.toDataURL();
      image.src = canvas.toDataURL();
      console.log(image.width)
      katexEquation.style.display = 'none'
      return equationImage
    })
  }
  return equationImage
};

export default generateEquationImage;
